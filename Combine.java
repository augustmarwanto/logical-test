/*
 * Combine.java
 * test
 *
 * Created by Agus Marwanto
 * on Nov 17, 2021
 */

package test;

import java.util.HashMap;

public class Combine {

	public static void main(String[] args) {

		HashMap<String, Integer> objA = new HashMap<>();
		objA.put("a", 10);
		objA.put("b", 20);
		objA.put("c", 30);

		HashMap<String, Integer> objB = new HashMap<>();
		objB.put("a", 3);
		objB.put("c", 6);
		objB.put("d", 3);

		System.out.println(combine(objA, objB));

	}

	public static HashMap<String, Integer> combine(HashMap<String, Integer> objA,
			HashMap<String, Integer> objB) {
		objB.forEach((k, v) -> objA.merge(k, v, (oldValue, newValue) -> oldValue + newValue));
		return objA;
	}

}
